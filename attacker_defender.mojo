<?xml version="1.0" encoding="UTF-8"?>
<project name="attacker_defender" board="Mojo V3" language="Lucid">
  <files>
    <src>boolFunction.luc</src>
    <src>shifterFunction.luc</src>
    <src>cmpFunction.luc</src>
    <src top="true">mojo_top.luc</src>
    <src>adderFunction.luc</src>
    <src>alu.luc</src>
    <src>memory.luc</src>
    <ucf>custom.ucf</ucf>
    <ucf lib="true">mojo.ucf</ucf>
    <component>reset_conditioner.luc</component>
  </files>
</project>
